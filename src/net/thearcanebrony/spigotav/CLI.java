package net.thearcanebrony.spigotav;

import org.fusesource.jansi.AnsiConsole;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarFile;

public class CLI {
    public static void main(String[] _args)
    {
        AnsiConsole.systemInstall();
        String baseDir = ".";
        List<String> args = Arrays.asList(_args);
        int arg = 0;
        if(args.contains("--help")){
            System.out.println("SpigotAV help\n" +
                    "Supported arguments:\n" +
                    " --help\t\tShows this message\n" +
                    " --html\t\tExport results as HTML\n" +
                    " --verbose\tIncrease verbosity\n" +
                    " --cd\t\tChange current directory\n" +
                    " --launch\tLaunches a server, next argument: jar path (will be executed in current dir!)\n" +
                    " --dir\t\tDirectory to scan (defaults to current directory)");
            return;
        }
        if(args.contains("--verbose")){
            ConUtil.println("Option: Verbose");
            BaseConfig.Verbose = true;
        }
        if((arg = args.indexOf("--cd")) >= 0){
            String newpath = args.get(arg+1);
            if(BaseConfig.Verbose) ConUtil.println("Option: Change current directory: "+ newpath);
            if(Files.exists(Path.of(newpath))){
                System.setProperty("user.dir", Path.of(newpath).toAbsolutePath().toString());
            }
            else {
                System.out.println("Invalid argument --cd: Directory does not exist");
                return;
            }
        }
        if((arg = args.indexOf("--dir")) >= 0){
            if(Files.exists(Path.of(args.get(arg+1)))){
                baseDir = args.get(arg+1);
            }
            else {
                System.out.println("Invalid argument --dir: Directory does not exist");
                return;
            }
        }
        if(args.contains("--html")){
            if(BaseConfig.Verbose) ConUtil.println("Option: dump html");
            BaseConfig.DumpHtml = true;
        }
        Scan.Scan(baseDir, true);
        if((arg = args.indexOf("--launch")) >= 0){
            if(Files.exists(Path.of(args.get(arg+1)))){
                try {
                    JarFile serverJar = new JarFile(args.get(arg+1));
                    String MainClass = serverJar.getManifest().getMainAttributes().getValue(Attributes.Name.MAIN_CLASS);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                System.out.println("Invalid argument --launch: File/directory does not exist");
                return;
            }

        }
        //run server

//        Class classToLoad = Class.forName("com.MyClass", true, child);
//        Method method = classToLoad.getDeclaredMethod("myMethod");
//        Object instance = classToLoad.newInstance();
//        Object result = method.invoke(instance);
    }
}
