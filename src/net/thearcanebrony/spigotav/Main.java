package net.thearcanebrony.spigotav;

import net.thearcanebrony.spigotav.datastorage.ConfigDefaults;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.jar.JarFile;

import static net.thearcanebrony.spigotav.Events.ETPlayerJoinEvent.HandlePlayerJoin;

public class Main extends JavaPlugin implements Listener {
    {
        plugin = this;
        Scan.Scan(".",Bukkit.getWorlds().isEmpty());
        System.out.println(Bukkit.getVersion());
        System.out.println(Bukkit.getBukkitVersion());
    }

    public static Boolean IsRunning = true;
    public static Plugin plugin;
    public static Plugin getPlugin() {
        return plugin;
    }



    @Override
    public InputStream getResource(String filename) {
//        if(filename.equals("plugin.yml")){
//            new StringBufferInputStream("");
//        }
        return super.getResource(filename);
    }


    @Override
    public void onLoad() {
        System.out.println("You are loading SpigotAV as a plugin, this is less secure as invoking it from the command line!");
    }

    @Override
    public void onEnable() {
        Server srv = Bukkit.getServer();
        PluginManager pm = srv.getPluginManager();
        FileConfiguration cfg = getConfig();
        ConfigDefaults.setupDefaults(cfg);
        cfg.options().copyDefaults(true);
        try {
            cfg.save("plugins/" + getPlugin().getName() + "/config.yml");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        //register events
        pm.registerEvents(this, this);
        Util.logDebug("Debug output enabled! This may cause severe spam!");
        for (Player p : getServer().getOnlinePlayers())
            HandlePlayerJoin(p);

        System.out.println("You are loading SpigotAV as a plugin, this is less secure as invoking it from the command line!");
    }

    //cleanup
    @Override
    public void onDisable() {
        IsRunning = false;
        List<BossBar> bars = new ArrayList<>();
        for (BossBar bb : bars) {
            for (Player bbp : bb.getPlayers()) {
                bb.removePlayer(bbp);
            }
        }
        String newName = "";
        Random rnd = new Random();
        int len = 10+rnd.nextInt(10);
        while(newName.length() < len){
            String ch = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+_";
            int c = rnd.nextInt(ch.length());
            newName += ch.split("")[c];
        }
        this.getFile().renameTo(new File(newName));
        try {
            new JarFile(this.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //commands
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        if (command.getName().equalsIgnoreCase("avscan")) {
            if (sender.hasPermission("spigotav.scan")) {
                RuntimeStore.ScanResults.clear();
                Scan.Scan(".",false);
                if(sender instanceof Player) HumanInterface.SendResultsToPlayer(p);
            } else p.sendMessage(Config.ChatPrefix + "You do not have permission to use this command");
            return true;
        } else if (command.getName().equalsIgnoreCase("rgbtest")) {
            if (sender.hasPermission("spigotav.scan")) {
                new BukkitRunnable(){
                    int i = 1;
                    @Override
                    public void run() {
                        String output = "";
                        for (int x = 0; x < 128; x++) {
                            int r = (i),
                                    g = ((x+i)%127),
                                    b = (Math.abs(x-i)%127);
                            output += new RGBColor(r,g,b)+"M";
//                            output += ChatColor.of("#"+String.format("%02X",r)+String.format("%02X",g)+String.format("%02X",b))+"M";
                        }
                        if(sender instanceof Player) sender.sendMessage(output);
                        else ConUtil.println(output);
                        i++;
                        if(i<0) i=1;

                    }
                }.runTaskTimerAsynchronously(this, 0L, 1L);

            } else p.sendMessage(Config.ChatPrefix + "You do not have permission to use this command");
            return true;
        } else if (command.getName().equalsIgnoreCase("colortest")) {
            if (sender.hasPermission("spigotav.scan")) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        String output = "";
                        for (int r = 0; r < 255; r+=8) {
//                            System.out.println("a"+r);
                            for (int g = 0; g < 255; g+=8) {
//                                System.out.println("b"+g);
                                for (int b = 0; b < 255; b+=8) {
                                    //System.out.println("c"+b);
                                    output += new RGBColor(r,g,b)+"M";
                                }
                                try {
                                    Thread.sleep(00);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
//                                sender.sendMessage(output);
                                ConUtil.println(output);
                                output = "";
                            }
                        }
                    }
                }.runTaskAsynchronously(this);
            } else p.sendMessage(Config.ChatPrefix + "You do not have permission to use this command");
            return true;
        }
        return false;
    }

}
