package net.thearcanebrony.spigotav;

import net.md_5.bungee.api.ChatColor;
import net.thearcanebrony.spigotav.datastorage.ScanResult;
import org.bukkit.entity.Player;
import org.jsoup.Jsoup;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDateTime;

public class HumanInterface {
    public static void SendResultsToPlayer(Player p) {
        p.sendMessage(GetResultsText());
    }

    public static void LogToConsole() {
        ConUtil.println(GetResultsText());
    }

    public static String GetResultsText() {
        String detections = new RGBColor(255,127,127)  + "------Malware detections------" + new RGBColor(255,255,255);

        if(RuntimeStore.ScanResults.size() > 0 && RuntimeStore.ScanResults.stream().anyMatch(x -> x.Suspicious)){
            for (ScanResult res : RuntimeStore.ScanResults) {
                if (res.Suspicious) {
                    detections += new RGBColor(255,0,0) + "\n - " + new RGBColor(200,200,200) + res.File +
                            new RGBColor(75,75,75) + "\n  -> " + res.getColor() + res.Detection;
                }
            }
        }
        else {
            detections = new RGBColor(0,255,0) + "No malware found!";
        }
//        System.out.println(detections);
        return detections + new RGBColor(255,255,255);
    }
    public static void ShowTitle(Player p){
        ScanResult[] sus = RuntimeStore.ScanResults.stream().filter(x -> x.Suspicious).toArray(ScanResult[]::new);
        p.sendTitle(ChatColor.DARK_RED +"MALWARE FOUND",
                ChatColor.RED.toString()+sus.length+"/"+RuntimeStore.ScanResults.size()+" plugins infected!",
                0,100,40);
    }
    public static void DumpHtml(){
        ConUtil.println("Writing HTML file: SpigotAV.html...");
        try {
            FileWriter fw = new FileWriter("SpigotAV.html");
            String style = "<style>\nbody{background-color: #000;font-family: monospace;}\n" +
                    "h1, a, b, p {color: #fff;}\n" +
                    ".collapsible {\n" +
                    "  background-color: #222;\n" +
                    "  color: #444;\n" +
                    "  cursor: pointer;\n" +
                    "  padding: 18px;\n" +
                    "  width: 100%;\n" +
                    "  border: none;\n" +
                    "  text-align: left;\n" +
                    "  outline: none;\n" +
                    "  font-size: 15px;\n" +
                    "}\n" +
                    ".active, .collapsible:hover {\n" +
                    "  background-color: #333;\n" +
                    "}\n" +
                    ".content {\n" +
                    "  padding: 0 18px;\n" +
                    "  display: none;\n" +
                    "  overflow: hidden;\n" +
                    "  background-color: #111;\n" +
                    "}\n</style>\n<script>\nfunction pageLoaded(){var coll = document.getElementsByClassName(\"collapsible\");\n" +
                    "var i;\n" +
                    "\n" +
                    "for (i = 0; i < coll.length; i++) {\n" +
                    "  coll[i].addEventListener(\"click\", function() {\n" +
                    "    this.classList.toggle(\"active\");\n" +
                    "    var content = this.nextElementSibling;\n" +
                    "    if (content.style.display === \"block\") {\n" +
                    "      content.style.display = \"none\";\n" +
                    "    } else {\n" +
                    "      content.style.display = \"block\";\n" +
                    "    }\n" +
                    "  });\n" +
                    "}\n" +
                    "}\n</script>";
            String detections = style + String.format("<body onload='pageLoaded'><h1>SpigotAV Scan results (%s)</h1><hr><h2 style='color: %s;'>Malware detections</h2><br>", LocalDateTime.now(), ScanResult.getColor(Util.getHighestDetectionLevel()).toHex());
            String clean = "<br><button type=\"button\" class=\"collapsible\">Clean files</button>\n" +
                    "<div class=\"content\">\n";
            String legend = "<b>Legend:</b>";
            for(Field f : ScanResult.Severity.class.getDeclaredFields()){
                try {
                    legend += String.format("<a style='color:%s;'>%s</a>",ScanResult.getColor((int)f.get(null)).toHex(), f.getName());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            detections += legend;
            if(RuntimeStore.ScanResults.size() > 0){
                for (ScanResult res : RuntimeStore.ScanResults) {
                    if (res.Suspicious) {
                        detections += String.format(  "<p>\n" +
                                        "- %s<br>\n" +
                                        "  <a style='margin-left: 10px;'>-></a> <a style='color: %s;'>%s</a>\n" +
                                        "</p>\n", res.File, res.getColor().toHex(), res.Detection);
                    }
                    else {
                        clean += String.format("  <p>%s</p>\n", res.File);
                    }
                }
            }
            if(RuntimeStore.ScanResults.stream().noneMatch(x -> x.Suspicious)) {
                detections +=  String.format("<b style='color: %s;'>No malware found!</b>", new RGBColor(0,255,0).toHex());
            }


            detections += clean + "</div><script>pageLoaded();</script>";

            fw.write(Jsoup.parse(detections).toString());
            fw.flush();
            fw.close();
            ConUtil.println("Finished writing HTML file!");
        } catch (IOException e) {
            System.out.println("Failed to write HTML file!");
            e.printStackTrace();
        }
    }
}
