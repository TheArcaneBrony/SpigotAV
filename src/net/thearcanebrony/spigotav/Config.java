package net.thearcanebrony.spigotav;

public class Config {
    {
        BaseConfig.Silent = MCUtil.getConfig().getBoolean("Silent Console");
    }
    public static boolean LogDebug = MCUtil.getConfig().getBoolean("Debug.Log Debug Info");
    public static boolean SendOPs = MCUtil.getConfig().getBoolean("Warnings.OP");
    public static boolean SendEveryone = MCUtil.getConfig().getBoolean("Warnings.Everyone");
    public static boolean SendWithPerm = MCUtil.getConfig().getBoolean("Warnings.With Permission");
    public static boolean WelcomeMessageEnabled = MCUtil.getConfig().getBoolean("Welcome Message.Enabled");
    public static String WelcomeMessage = MCUtil.getConfig().getString("Welcome Message.Text");
    public static String ChatPrefix = MCUtil.getConfig().getString("Chat Prefix");
}
