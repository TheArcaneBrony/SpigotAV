package net.thearcanebrony.spigotav;

import net.thearcanebrony.spigotav.datastorage.ScanResult;

import java.io.IOException;
import java.util.List;
import java.util.jar.JarFile;
import java.util.zip.ZipException;

public class Scan {
    public static ScanResult ScanFile(String file) {
        boolean sus = false;
        String det = "";
        try {
            if(BaseConfig.Verbose) System.out.println("Scanning " + file);
            JarFile jarfile = new JarFile(file);
            if (jarfile.getJarEntry("javassist") != null) {
                sus = true;
                if (jarfile.getJarEntry(".l1") != null || jarfile.getJarEntry(".l_ignore") != null) {
                    det = "worm.backdoor.Rafael10";
                } else {
                    det = "pul.javassist";
                }
            }
        } catch (ZipException e){
            sus = true;
            det = "corrupted_jar";
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return new ScanResult(file, sus, det);
    }
    public static void Scan(String dir, boolean HaltOnDetection) {
        System.out.println("Scan started!");
        try {
            ConUtil.println("Scanning "+ dir + "...");
            List<String> files = FSUtil.findAllJars(dir, true);
            ConUtil.println("Scanning JRE...");
            files.addAll(FSUtil.findAllJars(System.getProperty("java.home"), true));
            System.out.println("Scanning " + files.size() + " jars...");
            for (String file : files) {
                if (file.endsWith(".jar")) {
                    RuntimeStore.ScanResults.add(ScanFile(file));
                }
            }
            if (RuntimeStore.ScanResults.stream().anyMatch(x->x.Suspicious)) {
                try {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("Malware found!!!");
                    }
                    /*if(Arrays.stream(Package.getPackages()).anyMatch(x->x.getName().contains("")))*/
                    HumanInterface.LogToConsole();
                    if (HaltOnDetection) {
                        System.out.println("Halting startup for 5 seconds...");
                        Thread.sleep(000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                ConUtil.println(new RGBColor(0,255,0) + "Nothing suspicious found!");
            }
            if(BaseConfig.DumpHtml) HumanInterface.DumpHtml();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

