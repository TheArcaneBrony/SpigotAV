package net.thearcanebrony.spigotav.datastorage;

import org.bukkit.configuration.file.FileConfiguration;

public class ConfigDefaults {
    public static void setupDefaults(FileConfiguration c) {
        c.addDefault("# Use ALT+21 for color codes!", 0);
        c.addDefault("Chat Prefix", "§6[SpigotAV]§r ");
        c.addDefault("Welcome Message.Text", "§6This server is running SpigotAV by The Arcane Brony!");
        c.addDefault("Welcome Message.Enabled", false);
        c.addDefault("Warnings.Everyone", false);
        c.addDefault("Warnings.OP", true);
        c.addDefault("Warnings.With Permission", true);
        c.addDefault("Debug.Log Events", false);
        c.addDefault("Debug.Log Debug Info", false);
        c.addDefault("Player Data.TheArcaneBrony.Commands On Join", new String[]{""});
        c.addDefault("Silent Console", false);
    }
}