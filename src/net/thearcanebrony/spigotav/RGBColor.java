package net.thearcanebrony.spigotav;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class RGBColor {
    public static Pattern ColorPattern = Pattern.compile("~#[0-F]{6}");
    //mc color mapping
    private static final Map<Integer, String> COLOR_MAP = new HashMap() {{
        this.put(11141120, "DARK_RED");
        this.put(16733525, "RED");
        this.put(16755200, "GOLD");
        this.put(16777045, "YELLOW");
        this.put(43520, "DARK_GREEN");
        this.put(5635925, "GREEN");
        this.put(5636095, "AQUA");
        this.put(43690, "DARK_AQUA");
        this.put(170, "DARK_BLUE");
        this.put(5592575, "BLUE");
        this.put(16733695, "LIGHT_PURPLE");
        this.put(11141290, "DARK_PURPLE");
        this.put(16777215, "WHITE");
        this.put(11184810, "GRAY");
        this.put(5592405, "DARK_GRAY");
        this.put(0, "BLACK");
    }};

    private static double getDistance(final RGBColor c1, final RGBColor c2) {
        final double r = c1.Red - c2.Red;
        final double g = c1.Green - c2.Green;
        final int b = c1.Blue - c2.Blue;
        return r * r + g * g + b * b;
    }
    public int Red = 0, Green = 0, Blue = 0;

    public RGBColor(int r, int g, int b) {
        Red = r%256;
        Green = g%256;
        Blue = b%256;
    }
    public RGBColor(String formattedHex){
        int[] data = Util.hexStringToByteArray(formattedHex.replace("~#", ""));
        Red = data[0];
        Green = data[1];
        Blue = data[2];
//        System.out.println(String.format("R: %s G: %s B: %s", Red, Green,Blue));
    }

    @Override
    public String toString() {
        return String.format("~#%02X%02X%02X", Red, Green, Blue);
    }
    public String getAnsiColor() {
        return String.format("\033[38;2;%s;%s;%sm", Red, Green, Blue);
    }
    public String toHex() {
        return String.format("#%02X%02X%02X", Red, Green, Blue);
    }
    public String getLegacyColor(){
        return getLegacyColor(this);
    }
    public static String getLegacyColor(final RGBColor color) {
        double shortestDistance = 256 * 256 * 256;
        String currentTarget = "BLACK";
        for (final Map.Entry<Integer, String> entry : COLOR_MAP.entrySet()) {
            Color _cl = new Color(entry.getKey());
            final RGBColor cl = new RGBColor(_cl.getRed(), _cl.getGreen(), _cl.getBlue());
            final double dist = getDistance(color, cl);
            if (dist < shortestDistance) {
                shortestDistance = dist;
                currentTarget = entry.getValue();
            }
        }
        return currentTarget;
    }

}
